const fs = require("fs");

/********/

/*
 * A Joined object provides an abstract way of combining a list of items, typically by concatenating them into a single
 * string, without having to worry (much) about how to wrap each item.
 *
 */
function Joined(...args) {
    const arr = [];

    /*
     * Don't blindly join the given arguments as-is, as that's inefficient.
     *
     * Try to flatten all arguments first.
     *
     */

    for (const an_arg of args) {
        if (Joined.isJoined(an_arg) && an_arg.isFlattenAllowed()) {
            arr.push(...an_arg.getItems());
        } else {
            arr.push(an_arg);
        }
    }

    this.allow_flatten = true;
    this.items = arr;
}

Joined.isJoined = function(obj) {
    return typeof obj === "object" && obj.constructor === Joined;
};

Joined.prototype.getItems = function() {
    return this.items;
};

Joined.prototype.isFlattenAllowed = function() {
    return this.allow_flatten;
};

Joined.prototype.preventFlatten = function() {
    this.allow_flatten = false;
};

Joined.prototype.toString = function() {
    let str = "";

    for (const an_item of this.items.map(replaceBlockCommentsAndWhitespace)) {
        const middle = an_item.slice(1, an_item.length - 1);

        if (an_item.search(/^`[^`]*`$/) === 0 && [...middle].every(c => ALLOWED_ALPHABET.includes(c))) {
            /*
             * Naively unwrap strings whose "middle" characters are all in the allowed alphabet. This is usually safe,
             * but there are some special cases where this could cause problems, for instance if the first middle
             * character is "{" and an outside "$" appears before it. Such scenarios are not present at the moment,
             * however.
             *
             */
            str += middle;
        } else {
            str += ip(an_item);
        }
    }

    if (!this.allow_flatten) {
        str = preventFlatten(str);
    }

    return bt(str);
};

/********/

function join(...args) {
    return new Joined(...args);
}

/*
 * Use to prevent optimization logic from affecting function arguments (for instance to avoid having one argument being
 * broken up into two).
 *
 */
function preventFlatten(obj) {
    if (Joined.isJoined(obj)) {
        obj.preventFlatten();
    } else {
        obj = FLATTENING_INHIBITOR_CHAR + obj;
    }

    return obj;
}

function btip(...args) {
    if (args.length === 1) {
        return bt(ip(args[0]));
    } else if (args.length === 2) {
        const [prefix, an_arg] = args;

        if (typeof prefix === "string") {
            return bt(prefix + ip(an_arg));
        } else {
            throw Error("btip prefix argument must be a string.");
        }
    } else {
        throw Error("btip needs at least one and at most two arguments.");
    }
}

function bt(arg) {
    return "`" + arg + "`";
}

function ip(arg) {
    return "${" + arg + "}";
}

/*
 * Recursively flatten the provided argument list.
 *
 */
function flatten(...args) {
    if (typeof Array.prototype.flat === "function") {
        // ES2019
        return args.flat(Infinity);
    }

    let res = [];

    for (const an_arg of args) {
        if (Array.isArray(an_arg)) {
            res = res.concat(flatten(...an_arg));
        } else {
            res = res.concat(an_arg);
        }
    }

    return res;
}

/*
 * Generate a string of the form:
 *
 * `<arg_a>${<arg_b>}<arg_c>[...]${<arg_x>}`
 *
 * or:
 *
 * `<arg_a>${<arg_b>}<arg_c>[...]${<arg_x>}<arg_y>`
 *
 * For instance, in the second case, the actual order that the arguments will be consumed (when invoking a function
 * f in this way: f`<arg_a><...><arg_y>`) is:
 *
 * - first, the "in between" arguments: arg_a, arg_c, and so on up to and including arg_y, and
 * - then, the "dollar" arguments: arg_b etc. up to and including arg_x.
 *
 * Consider a smaller example with the three arguments arg_1, arg_2, and arg_3; they should be organized like this:
 *
 * `<arg_1>${<arg_3>}<arg_2>`
 *
 * What a function would actually receive in that case is:
 *
 * - Argument 1: [ "<arg_1>", "<arg_2>" ] (array that contains only strings)
 * - Argument 2: <arg_3> (not necessarily a string - this could be of any type)
 *
 * Note that the "concat" function may be applied to an empty array in order to flatten this like so: (see e.g.
 * the "pieces_of_replacer_expression" variable)
 *
 * [ "<arg_1>", "<arg_2>", <arg_3> ]
 *
 */
function interleave(args_raw, args_to_not_quote = []) {
    const args = flatten(args_raw);

    let res = "";

    const mid_point = Math.ceil(args.length / 2);
    const first_half = args.slice(0, mid_point);
    const second_half = args.slice(mid_point);

    for (let i = 0; i < mid_point; ++i) {
        res += first_half[i];

        const an_arg = second_half[i];

        // The caller of this function may allow an argument list with an odd length, so second_half[i] may be missing.
        if (an_arg !== undefined) {
            if (args_to_not_quote.includes(an_arg)) {
                // This argument is trusted to be pre-quoted - don't call the "bt" function.
                res += ip(an_arg);
            } else {
                res += ip(bt(an_arg));
            }
        }
    }

    return bt(res);
}

function createFunction(body, arg_names = []) {
    if (!Array.isArray(arg_names)) {
        throw Error("arg_names is not an array.");
    }

    if (body !== undefined) {
        if (!Joined.isJoined(body) && body !== __obj_an_Array && body !== BOUND_FIRST_STAGE) {
            /*
             * If the function body is not a Joined instance, this makes it trickier to reason about how to encapsulate
             * each argument. Exceptions are made for:
             *
             * - the dummy value "[]", as arguments can only be supplied together with a body and this serves as a
             *   minimal way to fulfill that requirement, and
             *
             * - output from the "getBoundFirstStage" function (which is generated using callFunction instead of join).
             *
             */
            throw Error("The function body must be a Joined instance (except for '[]' or the first stage).");
        }

        if (arg_names.length % 2 !== 0) {
            // Add a dummy argument for technical reasons.
            arg_names.push("");
        }

        const args_and_body = [...arg_names, body];
        const second_half_idx = Math.ceil(args_and_body.length / 2);

        if (arg_names.length % 2 === 0 && arg_names.every(
            (x, idx) => {
                if (typeof x === "string") {
                    // Allow any argument name to consist solely of one or more dollar signs.
                    const is_type_1 = x.match(/^[$]+$/) !== null;

                    // Allow the last argument name to be empty (results in a trailing comma).
                    const is_type_2 = idx === arg_names.length - 1 && x === "";

                    if (is_type_1 || is_type_2) {
                        return true;
                    }
                }

                return false;
            }
        )) {
            /*
             * Use the magic of ES2015 literals to create a function with the specified arguments and function
             * body.
             *
             * Callers (mostly) don't need to consider how this works, only to supply a supported argument list.
             *
             */

            // The body is already quote - don't requote it.
            const special_args = [body];

            return __obj_Function + interleave(args_and_body, special_args);
        } else {
            throw Error("Invalid argument list.");
        }
    } else {
        if (arg_names.length > 0) {
            throw Error("Arguments provided when body is not.");
        }

        return __obj_Function + bt("");
    }
}

function callFunction(body, args = []) {
    let arg_names = [];
    let arg_values = [];

    if (!Array.isArray(args)) {
        throw Error("args is not an array.");
    }

    if (args.length > 0) {
        const first_arg = args[0];

        if (typeof first_arg === "string") {
            arg_names.push(first_arg);
            // arg_values shouldn't have anything for the first argument as it's never consumed.
        } else {
            throw Error("First argument is invalid.");
        }

        for (const an_arg of args.slice(1)) {
            if (Array.isArray(an_arg) && an_arg.length === 2 && typeof an_arg[0] === "string") {
                arg_names.push(an_arg[0]);
                arg_values.push(an_arg[1]);
            } else {
                throw Error("One or more arguments (after the first argument) are invalid.");
            }
        }
    }

    /*
     * preventFlatten is used for each argument in order to ensure they aren't blended into each other by the join
     * function.
     *
     */
    return createFunction(body, arg_names) + join(...arg_values.map(an_arg => preventFlatten(an_arg)));
}

function fill(x) {
    let str = "";

    for (let i = 0; i < x; ++i) {
        // Fill with "random" characters just to be a bit more original than repeating a single character.
        str += "<{}[]"[i % 5];
    }

    return str;
}

function replaceBlockCommentsAndWhitespace(obj) {
    return obj
        .toString()

        // Remove all whitespace
        .replace(/\s+/mg, "")

        // Replace /*space*/ with space
        .replace(/\/\*space\*\//g, " ")

        // Remove all block comments
        .replace(/\/\*.*?\*\//g, "")
    ;
}

/********/

function getFirstStageTemplate() {
    /*
     * This and similar tricks is used throughout this function:
     *
     * Since Number(__str_concat__) - i.e. Number("concat") - is NaN, and:
     *
     * - x << NaN === x << 0, and
     * - NaN << y === 0 << y, and thus
     * - NaN << NaN === 0,
     *
     * this makes some subtle output size optimizations possible.
     *
     */

    /*
     * The coding mechanism works by first identifying the actual function body, and only then going through it
     * character by character to put in encoded versions (such as using the precalculated _c for "c").
     *
     */

    const char_map = new Map(Object.entries({
        " ": __str_space,
        "$": bt("$"),
        /*
         * Parentheses are expensive, so put markers in place. A lot of logic is used to replace all of them, in one
         * go, which is much cheaper. See comments in the "getBoundFirstStage" function.
         *
         */
        "(": bt(FIRST_STAGE_LP_MARKER),
        ")": bt(FIRST_STAGE_RP_MARKER),
        "<": bt("<"),
        "[": bt("["),
        "]": bt("]"),
        "c": _c,
        "f": _f,
        "i": _i,
        "n": _n,
        "o": _o,
        "r": _r,
        "t": _t,
        "u": _u,
        "{": bt("{"),
        "}": bt("}"),
    }));

    const replaceVarNames = str =>
        /*
         * Variable (and argument) names are assigned based primarily on frequency of use.
         *
         * Cheaper names go to the most used variables first. Only dollar signs are used.
         *
         * In one case, the same variable name is used twice: within obj_recursiveFunction, str_l is not needed, so the
         * corresponding variable name can be reused for obj_codeURIAndOutputString. (FIRST_STAGE_ARG_L and
         * FIRST_STAGE_OBJ_CODEURI_AND_OUTPUT_STRING are identical.)
         *
         * Note: "foo".replace("foo", "$$") won't work as the dollar sign has a special meaning for the "replace"
         * function. By supplying _ => X instead of just X, the literal value of each constant will be used.
         *
         */

        str
            .replace(/__str_concat__/g, _ => FIRST_STAGE_ARG_CONCAT)
            .replace(/__str_constructor_wrapped__/g, _ => FIRST_STAGE_ARG_CONSTRUCTOR_WRAPPED)
            .replace(/__obj_1__/g, _ => FIRST_STAGE_ARG_1)
            .replace(/__str_2e1__/g, _ => FIRST_STAGE_ARG_2E1)
            .replace(/__str_l__/g, _ => FIRST_STAGE_ARG_L)
            .replace(/__obj_codeURIAndOutputString__/g, _ => FIRST_STAGE_OBJ_CODEURI_AND_OUTPUT_STRING)
            .replace(/__str_returnAndSpace__/g, _ => FIRST_STAGE_ARG_RETURN_AND_SPACE)
            .replace(/__str_name__/g, _ => FIRST_STAGE_ARG_NAME)
            .replace(/__obj_recursiveFunction__/g, _ => FIRST_STAGE_OBJ_RECURSIVE_FUNCTION)
            .replace(/__str_to__/g, _ => FIRST_STAGE_ARG_TO)
            .replace(/__obj_secondStage__/g, _ => FIRST_STAGE_ARG_SECOND_STAGE)
            .replace(/__str_d__/g, _ => FIRST_STAGE_ARG_D)
            .replace(/__obj_userCode__/g, _ => FIRST_STAGE_ARG_USER_CODE)
    ;

    let increasinglyDemacroedString = `
        /*
         * Template for an immediately invoked recursive function to decode and call the second stage, wrapped in an
         * array to make it a function expression and not a function statement.
         *
         */
        [function /*space*/ __obj_recursiveFunction__(__obj_codeURIAndOutputString__) {

            /*
             * Loop four times (once for each char in "name")
             *
             * Parentheses are needed around __str_name__ as whitespace is stripped out by default.
             *
             */
            for ([] of (__str_name__)) /* { */

                /*
                 * for (x of [y]) { <statements> } can be considered to be the same as: x = y; <statements>;
                 *
                 * In other words:
                 *
                 * If ##__obj_index## is undefined, set it to zero, otherwise increase it by one.
                 *
                 * This works since undefined + 1 is NaN, and NaN << __str_concat__ is 0.
                 *
                 */
                for (##__obj_index## of [
                    ##__obj_Function##(
                        __str_returnAndSpace__
                            [__str_concat__](##__obj_index##)
                            [__str_concat__](##__str_plus##)
                            [__str_concat__](##__obj_1##)
                    )() << __str_concat__
                ])
                /* { */

                    /* Append the next two bits of the second stage to the string ##__obj_charCodeBits##. */
                    for (##__obj_charCodeBits## of [

                        /*
                         * Prefix the new value of ##__obj_charCodeBits## with its current value, or the empty string
                         * if it's undefined.
                         *
                         */
                        ##__obj_String##([##__obj_charCodeBits##])

                            /*
                             * Add the most significant bit - i.e. check if the character (##__obj_acc##) is lexically
                             * strictly lower than "concat". More to the point: is it in the second half of the four
                             * character alphabet (SECOND_STAGE_ALPHABET)?
                             *
                             * Context: SECOND_STAGE_ALPHABET is "{}][" and the ASCII order to consider is "[", "]",
                             * "concat", "{", and "}".
                             *
                             * Then, turn it into a number with a shift operation.
                             *
                             * Concatenate the result to a string, i.e. add "0" for "{" or "}" ; "1" for "]" or "[".
                             *
                             */
                            [__str_concat__]((##__obj_acc## < __str_concat__) << __str_concat__)

                            /*
                             * Add the least significant bit of this bit pair.
                             *
                             * This is done by checking if the character is at either end of the four character
                             * alphabet (index 0 or 3). Specifically, check if the characters fulfills at least one of
                             * these rules: (see definitions of condition 1 and 2 in comments below; note that both
                             * rules can't be fulfilled simultaneously for any of the four characters in the alphabet)
                             *
                             * a) The character *fails* condition 1, thus it's <= "[object Object]", which is only the
                             *    case for "[", or:
                             *
                             * b) the character *fulfills* condition 2, thus it's strictly higher than "{xxx" (where
                             *    each "x" can be assumed to be any ASCII character), which is only the case for "}".
                             *
                             * The resulting boolean is turned into a number with a shift operation, and is also added
                             * as a string: "0" or "1"
                             *
                             * Some parentheses are intentionally skipped as operator priority "happens" to allow it.
                             *
                             * The end result is that one of the following bit pairs is generated in this iteration:
                             *
                             * "{" : "00"
                             * "}" : "01"
                             * "]" : "10"
                             * "[" : "11"
                             *
                             * Notice that this corresponds to the placements (indexes 0-3) of these characters in the
                             * second stage alphabet.
                             *
                             */
                            [__str_concat__](
                                (
                                    /* ( */

                                        /* Condition 1 - is the character strictly higher than "[object Object]" ? */
                                        {} < ##__obj_acc##

                                    /* ) */

                                    <

                                    /* ( */

                                        ##__obj_1##

                                        <<

                                        /* Condition 2 - is the character strictly higher than "{xxx" ? */
                                        (__obj_secondStage__ < ##__obj_acc##)

                                    /* ) */
                                ) << __str_concat__
                            )

                    ]) {}

                /* } */

            /* } */

            /*
             * If ##__obj_charCodeBits## is "00000000", it must be because the entire next stage code has been read.
             *
             * This works because the the only place in the second stage where this code is present is at the very end,
             * where the null character is, or more precisely at the very beginning.
             *
             */

            if (##__obj_charCodeBits## < __obj_1__) {
                /*
                 * As __obj_recursiveFunction__ is ... recursive, a significant amount of items have been added to the
                 * stack at this point, and thus less stack will be available for the user code. It would be easy to
                 * clear much of the stack by delaying one or more of the ##__obj_Function## invocations, however that
                 * would come at the terrible cost of outputting a slightly larger program.
                 *
                 * The evaluation of the second stage has to be done in several substages due to the way it is
                 * constructed. In roughly inside-to-outside order:
                 *
                 * ##__obj_outputString## contains the URI-encoded version of the second stage, so for instance "o"
                 * would be represented by "%6f". The ##__obj_decodeURI## call deals with this. Note that "return "
                 * is then prefixed before the innermost ##__obj_Function## call is made, as this keyword is omitted in
                 * the second stage for space optimization reasons. The resulting function is then executed to trigger
                 * the "return <...>" code.
                 *
                 * That returns yet another function body - except for another "return " which is prefixed here - so
                 * now the parent ##__obj_Function## call can be made, and the resulting function executed to trigger
                 * the "return <...>" code. The returned function (lambda) can be said to be the *actual* second stage.
                 *
                 * The user code is then supplied to the second stage as a function argument.
                 *
                 * Once the second stage returns the fully decoded user code, it is evaluated by the top-level
                 * ##__obj_Function## call and then the resulting function is executed.
                 *
                 * Assuming the user code exits, control returns to the subsequent if statement, which fails, recursion
                 * stops, and the program terminates.
                 *
                 */

                ##__obj_Function##(

                    ##__obj_Function##(

                        __str_returnAndSpace__
                            [__str_concat__](

                                ##__obj_Function##(

                                    __str_returnAndSpace__
                                        [__str_concat__](

                                            ##__obj_decodeURI##(##__obj_outputString##)

                                        )

                                )()

                        )

                    )()

                    /* Supply the user code to the second stage */

                    (__obj_userCode__)

                )()

            }

            /* Cheaper than adding "else" or "return". Effectively the same as: if (+##__obj_charCodeBits## !== 0) */

            if (##__obj_charCodeBits## << __str_concat__) /* { */

                /*
                 * - Set ##__obj_charCodeBits## to ""-ish (implicit, see macro definition for details).
                 *
                 * - Create an array where ##_codeURI## is reused as the first element.
                 *
                 * - Provide an updated ##__obj_outputString## by concating it to this new array.
                 *
                 * - Recurse by calling __obj_recursiveFunction__ with the new array.
                 *
                 */

                __obj_recursiveFunction__(

                    [##_codeURI##]
                        [__str_concat__](

                            /* Prepend %xx to the new ##__obj_outputString## */
                            ##__str_percent##
                                [__str_concat__](

                                    /*
                                     * Add the character code in hexadecimal digits (no padding needed; chars < 16 are
                                     * not present, not even the null character as the if statement guards against it).
                                     *
                                     * Same as: Number("0bxxxxxxxx").toString(16)
                                     *
                                     */

                                    (
                                        ##__obj_String##(##__obj_0##)
                                            [__str_concat__](##_b##)
                                            [__str_concat__](##__obj_charCodeBits##)

                                        << __str_concat__
                                    )
                                        [##_toString##](##__obj_16##)

                                )

                                /*
                                 * If ##__obj_outputString## is undefined (the starting value), then this becomes
                                 * "%xx".concat([undefined]) which is the same as: "%xx"
                                 *
                                 */

                                [__str_concat__]([##__obj_outputString##])

                        )

                )

            /* } */

        }(

            [

                ##_c##
                    [__str_concat__](##_o##)
                    [__str_concat__](##_d##)
                    [__str_concat__](##_e##)
                    [__str_concat__](##_U##)
                    [__str_concat__](##_R##)
                    [__str_concat__](##_I##)

                /* The second element in the array is intentionally skipped. */

            ]

        )]
    `;

    while (true) {
        const oldString = increasinglyDemacroedString;

        increasinglyDemacroedString = increasinglyDemacroedString.replace(/##([^#]+)##/g,
            (_, tag) => {
                let res = "";

                if (tag === "__obj_0") {
                    res = `__str_concat__ << __str_concat__`;
                } else if (tag === "__obj_1") {
                    res = `__obj_1__`;
                } else if (tag === "__obj_2") {
                    res = `##__obj_1## << ##__obj_1##`;
                } else if (tag === "__obj_4") {
                    res = `##__obj_1## << ##__obj_2##`;
                } else if (tag === "__obj_8") {
                    res = `##__obj_2## << ##__obj_2##`;
                } else if (tag === "__obj_16") {
                    res = `##__obj_1## << [##__obj_4##]`;
                } else if (tag === "__obj_20") {
                    res = `__str_2e1__ << __str_concat__`;
                } else if (tag === "__obj_32") {
                    res = `##__obj_16## << ##__obj_1##`;
                } else if (tag === "__obj_40") {
                    res = `__str_2e1__ << ##__obj_1##`;
                } else if (tag === "__obj_Function") {
                    /*
                     * Shorter than __obj_recursiveFunction__[__str_constructor_wrapped__], and more generic (this is
                     * also used when __obj_recursiveFunction__ is not defined).
                     *
                     */
                    res = `__str_concat__[__str_constructor_wrapped__][__str_constructor_wrapped__]`;
                } else if (tag === "__obj_Infinity") {
                    res = `
                        ##__obj_Number##(
                            ##_2e11048576##
                        )
                    `;
                } else if (tag === "__obj_Number") {
                    res = `__obj_1__[__str_constructor_wrapped__]`;
                } else if (tag === "__obj_String") {
                    res = `__str_concat__[__str_constructor_wrapped__]`;
                } else if (tag === "__obj_acc") {
                    res = `__obj_secondStage__[##__obj_index##]`;
                } else if (tag === "__obj_charCodeBits") {
                    /*
                     * Get a bonus variable on this array by using a property that has the empty string ("") as its
                     * name. (If a "" property already exists on the prototype, this may fail.)
                     *
                     */
                    res = `__obj_codeURIAndOutputString__[[]]`;
                } else if (tag === "__obj_decodeURI") {
                    res = `
                        ##__obj_Function##(
                            __str_returnAndSpace__
                                [__str_concat__](##_decodeURI##)
                        )()
                    `;
                } else if (tag === "__obj_encodeURI") {
                    res = `
                        ##__obj_Function##(
                            __str_returnAndSpace__
                                [__str_concat__](##_encodeURI##)
                        )()
                    `;
                } else if (tag === "__obj_index") {
                    /*
                     * Get a bonus variable by using a "concat" property on this function. (If a "concat" property
                     * already exists on the prototype, this may fail.)
                     *
                     */
                    res = `__obj_recursiveFunction__[__str_concat__]`;
                } else if (tag === "__obj_outputString") {
                    res = `__obj_codeURIAndOutputString__[##__obj_1##]`;
                } else if (tag === "_2e11048576") {
                    res = `
                        __str_2e1__
                            [__str_concat__](__obj_1__ << __str_2e1__)
                    `;
                } else if (tag === "_2e120") {
                    res = `
                        __str_2e1__
                            [__str_concat__](##__obj_20##)
                    `;
                } else if (tag === "_401") {
                    res = `
                        ##__obj_String##(##__obj_40##)
                            [__str_concat__](##__obj_1##)
                    `;
                } else if (tag === "_I") {
                    res = `
                        ##__obj_String##(##__obj_Infinity##)
                            [##__obj_0##]
                    `;
                } else if (tag === "_R") {
                    /*
                     * Induce and catch a ReferenceError by trying to access "let".
                     *
                     * While this could technically exist on the global object, it seems far-fetched that it would
                     * do so; for instance, any modern JS engine must treat this as a SyntaxError in strict mode.
                     * (Strict mode is never requested here, but it would seem weird to define some property, with this
                     * name, that could only be accessed in non-strict mode.)
                     *
                     * A shorter but shakier version would be possible (just reference "concat"), or one could induce a
                     * RangeError instead (a safer but more expensive approach) using the equivalent of Array(NaN).
                     *
                     */
                    res = `
                        ##__obj_String##(
                            ##__obj_Function##(
                                /* try{let} */

                                ##_try##
                                    [__str_concat__](##__str_lcb##)
                                    [__str_concat__](##_let##)
                                    [__str_concat__](##__str_rcb##)

                                /* catch(concat) */

                                    [__str_concat__](##_catch##)
                                    [__str_concat__](##__str_lp##)
                                    [__str_concat__](__str_concat__)
                                    [__str_concat__](##__str_rp##)

                                /* {return concat} */

                                    [__str_concat__](##__str_lcb##)
                                    [__str_concat__](__str_returnAndSpace__)
                                    [__str_concat__](__str_concat__)
                                    [__str_concat__](##__str_rcb##)
                            )()
                        )
                            /* Get the "R" from "ReferenceError: [...]" */
                            [##__obj_0##]
                    `;
                } else if (tag === "_U") {
                    // Same as Object.prototype.toString.call(undefined)[8] or "[object Undefined]"[8]
                    res = `
                        {}[##_toString##][##_call##]()
                            [##__obj_8##]
                    `;
                } else if (tag === "_a") {
                    res = `__str_name__[##__obj_1##]`;
                } else if (tag === "_b") {
                    // "[object Object]"[2]
                    res = `
                        ##__obj_String##({})
                            [##__obj_2##]
                    `;
                } else if (tag === "_c") {
                    res = `__str_concat__[##__obj_0##]`;
                } else if (tag === "_call") {
                    res = `
                        ##_c##
                            [__str_concat__](##_a##)
                            [__str_concat__](##_l##)
                            [__str_concat__](##_l##)
                    `;
                } else if (tag === "_catch") {
                    res = `
                        ##_c##
                            [__str_concat__](##_a##)
                            [__str_concat__](##_t##)
                            [__str_concat__](##_ch##)
                    `;
                } else if (tag === "_ch") {
                    // A little cheaper than generating both "c" and "h" separately.
                    res = `
                        (##_401## << __str_concat__)
                            [##_toString##](##__obj_32##)
                    `;
                } else if (tag === "_codeURI") {
                    res = `__obj_codeURIAndOutputString__[##__obj_0##]`;
                } else if (tag === "_d") {
                    res = `__str_d__`;
                } else if (tag === "_decodeURI") {
                    res = `
                        ##_d##
                            [__str_concat__](##_e##)
                            [__str_concat__](##_codeURI##)
                    `;
                } else if (tag === "_e") {
                    res = `__str_2e1__[##__obj_1##]`;
                } else if (tag === "_encodeURI") {
                    res = `
                        ##_e##
                            [__str_concat__](##_n##)
                            [__str_concat__](##_codeURI##)
                    `;
                } else if (tag === "_l") {
                    res = `__str_l__`;
                } else if (tag === "_let") {
                    res = `
                        ##_l##
                            [__str_concat__](##_e##)
                            [__str_concat__](##_t##)
                    `;
                } else if (tag === "_n") {
                    res = `__str_name__[##__obj_0##]`;
                } else if (tag === "_name") {
                    res = `__str_name__`;
                } else if (tag === "_o") {
                    res = `__str_concat__[##__obj_1##]`;
                } else if (tag === "_r") {
                    res = `__str_returnAndSpace__[##__obj_0##]`;
                } else if (tag === "_t") {
                    res = `__str_to__[##__obj_0##]`;
                } else if (tag === "_toString") {
                    res = `
                        __str_to__
                            [__str_concat__](##__obj_String##[##_name##])
                    `;
                } else if (tag === "_try") {
                    res = `
                        ##_t##
                            [__str_concat__](##_r##)
                            [__str_concat__](##_y##)
                    `;
                } else if (tag === "_y") {
                    // "return function anonymous(\n) {\n\n}"[20]
                    res = `
                        __str_returnAndSpace__
                            [__str_concat__](##__obj_Function##())
                            [##__obj_20##]
                    `;
                } else if (tag === "__str_lcb") {
                    /*
                     * The second stage and its alphabet have been carefully manipulated to guarantee the first
                     * character of __obj_secondStage__ is a left curly bracket.
                     *
                     */
                    res = `__obj_secondStage__[##__obj_0##]`;
                } else if (tag === "__str_lp") {
                    // Left parenthesis from "tofunction anonymous(\n) {\n\n}"[20]
                    res = `
                        __str_to__
                            [__str_concat__](##__obj_Function##())
                            [##__obj_20##]
                    `;
                } else if (tag === "__str_percent") {
                    // "%5Bobject%20Object%5D"[0]
                    res = `
                        ##__obj_encodeURI##({})
                            [##__obj_0##]
                    `;
                } else if (tag === "__str_plus") {
                    // "2e+120"[2]
                    res = `
                        ##__obj_String##(
                            ##__obj_Number##(
                                ##_2e120##
                            )
                        )
                            [##__obj_2##]
                    `;
                } else if (tag === "__str_rcb") {
                    /*
                     * The user code alphabet has been carefully chosen to guarantee the first character is a right
                     * curly bracket. (User code of length 0 is prevented at encoding time.)
                     *
                     */
                    res = `__obj_userCode__[##__obj_0##]`;
                } else if (tag === "__str_rp") {
                    // Right parenthesis from "function anonymous(\n) {\n\n}"[20]
                    res = `
                        ##__obj_String##(##__obj_Function##())
                            [##__obj_20##]
                    `;
                } else {
                    throw Error(`Invalid tag ##${tag}##`);
                }

                return replaceBlockCommentsAndWhitespace(res);
            }
        );

        if (oldString === increasinglyDemacroedString) {
            // All ##__foo_bar## type macros have been replaced.
            break;
        }
    }

    const stage1_template = replaceVarNames(replaceBlockCommentsAndWhitespace(increasinglyDemacroedString));

    const stage1_pieces = [];

    for (const [idx, ch] of Object.entries(stage1_template)) {
        const value = char_map.get(ch);

        if (value !== undefined) {
            stage1_pieces.push(value);
        } else {
            console.warn(`Internal error - unmapped character with code = ${ch.charCodeAt(0)}.`);
            const idx_num = Number(idx);
            const ctx_idx_left = idx_num >= 10 ? idx_num - 10 : 0;
            const ctx_idx_right = idx_num + 11;
            console.warn(`Context: ${
                stage1_template
                .substring(ctx_idx_left, ctx_idx_right)
                .trimRight()
            }`);
            console.warn(`         ${" ".repeat(10)}^`);
            process.exit(1);
        }
    }

    return join(...stage1_pieces);
}

function getBoundFirstStage() {
    /*
     * General comment about this function:
     *
     * This is likely the most difficult part of the code, due to the number of layers and indirections. Be warned.
     *
     * The first stage, as returned by the "getFirstStageTemplate" function, contains markers instead of left and right
     * parentheses. This is because these are costly to manufacture in place; it would be necessary to use something
     * like []["sort"]["constructor"], invoke that, turn the result into a string ("function anonymous(\n) {\n\n}"),
     * add an index to get "(" or ")", and then repeat that for every occurrence of either character.
     *
     * These parentheses still need to be manufactured somewhere, but it can be done in a way so that the number of
     * parentheses used in "getFirstStageTemplate" has a negligible effect on output size (with the overhead for each
     * additional left or right parenthesis being a single byte).
     *
     * The markers are replaced here (see arg_str_functext). For instance:
     *
     * function func(arg) {}
     *
     * could be encoded as:
     *
     * function func1arg2 {}
     *
     * where "1" and "2" are markers for "(" and ")" respectively.
     *
     * The actual marker strings are defined by FIRST_STAGE_LP_MARKER and FIRST_STAGE_RP_MARKER. Each of these markers
     * is carefully chosen to ensure it constitutes a sequence of characters that cannot be used for any other reason
     * without producing a syntax error (except in string literals, which are not present in the template), thus
     * preventing accidental conflicts with other parts of the first stage template.
     *
     * Note that the following explanation is incomplete and simplified, for the sake of (relative...) brevity. Don't
     * take it too literally. See also the list of disclaimers at the end of this comment.
     *
     * In order to do the replacements (such as "1" with "(" and "2" with ")"), roughly the following approach is used:
     *
     * template["split"]("1")["join"]("(")["split"]("2")["join"](")")
     *
     * where the "template" variable is set to e.g. "function func1arg2 {}" as in the example above.
     *
     * (This is the same as template.replaceAll("1", "(").replaceAll("2", ")").)
     *
     * The strings "split", "join", and the material needed for "(" and ")" are (largely) precalculated, so a better
     * approximation is using variable names for these:
     *
     * template[split](lp_marker)[join](left_parenthesis)[split](rp_marker)[join](right_parenthesis)
     *
     * More practically: (using primarily in-alphabet characters, with a naive mapping of names)
     * $[$$]($$$)[$$$$]($$$$$)[$$]($$$$$$)[$$$$]($$$$$$$)
     *
     * There are still four pairs of parentheses that cannot be represented with the seven characters in the alphabet.
     * Yet another layer of indirection is therefore applied, so that all four pairs of parentheses can be derived from
     * a single string (the output from String(Function())).
     *
     * By using the following trick, the entire expression with all the "split" and "join" invocations can be built up
     * piece by piece without having to generate all the parentheses from scratch:
     *
     * []["concat"]`${"$"}${"$$"}${"$$$"}<...>`
     *
     * In this example, the concat function receives the arguments [ "", "" <...>, "", "" ], "$", "$$", "$$$" and so
     * on; the first argument with the array of empty strings represents all the empty strings before and after each
     * ${<...>}. All arguments are then concatenated in turn to the empty array, leaving:
     *
     * [ "", "" <...>, "", "", "$", "$$", "$$$", <...> ]
     *
     * By evaluating that array in turn with a Function invocation with the appropriate mappings for "$", "$$", "$$$",
     * etc., each piece of the expression with "split" and "join" calls can be built up.
     *
     * For example:
     *
     * - $ could be set to the string "$" (or "$$" or any other string, whatever is practical)
     * - $$ could be set to the string "[$$]"
     * - $$$ could be set to the string "("
     * - etc.
     *
     * An expression like this can now be created:
     * return[,,<...>,,$,$$,$$$,<...>]
     *
     * When evaluated, it becomes:
     * [, , <...>, , "$", "[$$]", "(", <...> ]
     *
     * After <...>["join"]`` (same as <...>["join"]([""]) or basically <...>["join"]("")) it becomes:
     * "$[$$](<...>"
     *
     * The resulting string, namely the aforementioned "$[$$]($$$)[$$$$]($$$$$)[$$]($$$$$$)[$$$$]($$$$$$$)", can then
     * be used as part of the body of a new function ("return $[$$]" etc.) that is executed with values for $, $$ and
     * so on, which then returns the first stage template with parenthesis markers replaced. The actual execution of
     * of the first stage is partly done in this function (using "bind") and partly in the "printProgram" function.
     *
     * As noted above, this is the general approach used, but the actual approach used below differs as it's using more
     * space-optimal mechanisms.
     *
     * Omitted optimizations include, but are not limited to:
     *
     * - interleaving arguments for concat (like `a${"d$"}b${"e"}c`) but with variable names that have dollar signs
     *   instead of letters,
     *
     * - adding e.g. "['join']" as a single piece instead of "[", "join" and "]", and
     *
     * - assigning argument names based on frequency of use (see e.g. the definitions of arg_str_functext and
     *   arg_get_join below),
     *
     * - carefully selecting which material is calculated "inline", and which material is "external" / precalculated
     *   (such as "constructor"), and
     *
     * - presupplying arguments that this function has access to but the "printProgram" function does not - see
     *   the comment in the beginning of the "pieces_of_replacer_expression" variable definition.
     *
     */

    /*
     * "super" arguments:
     *
     * These are not referenced directly (except in body_of_actual_first_stage_getter). For instance, to get the string
     * "to", "arg_to" is first referenced, which in turn references "super_arg_to", which in turn maps to the string
     * "to".
     *
     * With this indirection, it becomes possible to create constructs such as x["join"] by using x[$] where $ is
     * mapped to "join", instead of x[join] which would be a syntax error (unless "join" would be a name of a variable
     * name mapping to the string "join", which would be pointlessly redundant and expensive).
     *
     */
    const super_arg_constructor      = "$",
          super_arg_join             = "$$",
          super_arg_to               = "$$$",
          super_arg_e                = "$$$$",
          super_arg_na               = "$$$$$",
          super_arg_lp_marker        = "$$$$$$",
          super_arg_rp_marker        = "$$$$$$$",
          super_arg_names_string     = "$$$$$$$$",
          super_arg_stage            = "$$$$$$$$$",
          super_arg_bind             = "$$$$$$$$$$"
    ;

    const arg_str_functext     = "$",
          arg_1                = "$$",
          arg_32               = "$$$",
          arg_20               = "$$$$",
          arg_get_constructor  = "$$$$$",
          arg_24               = "$$$$$$",
          arg_get_join         = "$$$$$$$",
          arg_to               = "$$$$$$$$",
          arg_e                = "$$$$$$$$$",
          arg_na_prefixed      = "$$$$$$$$$$",
          arg_lp_marker        = "$$$$$$$$$$$",
          arg_rp_marker        = "$$$$$$$$$$$$",
          arg_names_string     = "$$$$$$$$$$$$$",
          arg_stage_suffixed   = "$$$$$$$$$$$$$$",
          arg_get_bind         = "$$$$$$$$$$$$$$$"
    ;

    /*
     * For the trick used with macro_obj_zeroish and macro_obj_0, see the explanation at the top of the
     * "getFirstStageTemplate" function.
     *
     */

    const macro_obj_zeroish = arg_str_functext;

    const macro_obj_0 = `${macro_obj_zeroish} << ${macro_obj_zeroish}`;

    const macro_obj_2 = `${arg_1} << ${arg_1}`;

    const macro_str_3 = `${arg_32}[${macro_obj_0}]`;

    const macro_obj_4 = `${macro_obj_2} << ${arg_1}`;

    const macro_obj_6 = `${macro_str_3} << ${arg_1}`;

    const macro_obj_8 = `${arg_1} << ${macro_str_3}`;

    // arg_str_functext yields "[]function anonymous($$$,$$$$$$\n) {\n\n}"

    const macro_str_lsb = `${arg_str_functext}[${macro_obj_0}]`; // Left square bracket

    const macro_str_rsb = `${arg_str_functext}[${arg_1}]`; // Right square bracket

    const macro_str_lp = `${arg_str_functext}[${arg_20}]`; // Left parenthesis

    const macro_str_comma = `${arg_str_functext}[${arg_24}]`;

    const macro_str_rp = `${arg_str_functext}[${arg_32}]`; // Right parenthesis

    const macro_seq_14 = [
        arg_1,
        macro_obj_4,
    ];

    const macro_seq_18 = [
        arg_1,
        macro_obj_8,
    ];

    const macro_seq_31 = [
        macro_str_3,
        arg_1,
    ];

    const macro_seq_26624131 = [
        macro_obj_2,
        macro_obj_6,
        macro_obj_6,
        arg_24,
        arg_1,
        macro_seq_31,
    ];

    const macro_seq_get_18 = [
        macro_str_lsb,
        macro_seq_18,
        macro_str_rsb,
    ];

    const macro_seq_get_20 = [
        macro_str_lsb,
        arg_20,
        macro_str_rsb,
    ];

    // Function()
    const macro_seq_func = [
        arg_1,
        arg_get_constructor,
        arg_get_constructor,
        macro_str_lp,
        macro_str_rp,
    ];

    // String(Function())
    const macro_seq_functext = [
        macro_str_lsb,
        macro_seq_func,
        macro_str_rsb,
        arg_get_join,
        macro_str_lp,
        macro_str_rp,
    ];

    // "m"
    const macro_seq_m = [
        macro_seq_functext,
        macro_str_lsb,
        macro_seq_14,
        macro_str_rsb,
    ];

    // ["na", "e"]["join"]("m")
    const macro_seq_name = [
        arg_na_prefixed,
        macro_str_comma,
        arg_e,
        macro_str_rsb,
        arg_get_join,
        macro_str_lp,
        macro_seq_m,
        macro_str_rp,
    ];

    const macro_seq_String = [
        arg_to,
        arg_get_constructor,
        macro_str_lsb,
        macro_seq_name,
        macro_str_rsb,
    ];

    // [<empty>, "String"]["join"]("to")
    const macro_seq_toString = [
        macro_str_lsb,
        macro_str_comma,
        macro_seq_String,
        macro_str_rsb,
        arg_get_join,
        macro_str_lp,
        arg_to,
        macro_str_rp,
    ];

    // Base 31 numbers - the unsung heroes
    const macro_seq_split = [
        macro_seq_26624131,
        macro_str_lsb,
        macro_seq_toString,
        macro_str_rsb,
        macro_str_lp,
        macro_seq_31,
        macro_str_rp,
    ];

    // [2, 1]["join"]("e")
    const macro_seq_2e1 = [
        macro_str_lsb,
        macro_obj_2,
        macro_str_comma,
        arg_1,
        macro_str_rsb,
        arg_get_join,
        macro_str_lp,
        arg_e,
        macro_str_rp,
    ];

    // The order of the arguments in this macro has to match the first part of super_arg_names_string_value.
    const macro_seq_partial_arg_values = [
        /*
         * The "getFirstStageTemplate" function will in practice treat ["constructor"] as "constructor".
         *
         * See the code there for details.
         *
         */
        arg_get_constructor,
        macro_str_comma,

        arg_1,
        macro_str_comma,

        macro_seq_name,
        macro_str_comma,

        arg_to,
        macro_str_comma,

        macro_seq_2e1,
    ];

    const pieces_of_replacer_expression = `
        []
        [${_concat}]
        /*
         * ["constructor"]["constructor"](
         *     <argument names>,
         *     <stage>
         *         ["split"](<lp marker>)
         *         ["join"]("(")
         *         ["split"](<rp marker>)
         *         ["join"](")")
         * )["bind"](1, <...>)
         *
         * or actually []["constructor"]["constructor"]( etc. The "[]" prefix is in body_of_actual_first_stage_getter.
         *
         */
        ${interleave([
            /*
             * ["constructor"]["constructor"](<argument names>,
             *
             */

            arg_get_constructor,
            arg_get_constructor,
            macro_str_lp,

            arg_names_string,

            macro_str_comma,

            /*
             * <stage>["split"](<lp marker>)
             *
             */

            arg_stage_suffixed, // <stage>[
            macro_seq_split,
            macro_str_rsb,

            macro_str_lp,
            arg_lp_marker,
            macro_str_rp,

            // ["join"]("(")

            arg_get_join,
            macro_str_lp,
            macro_seq_functext,
            macro_seq_get_18,
            macro_str_rp,

            // ["split"](<rp marker>)

            macro_str_lsb,
            macro_seq_split,
            macro_str_rsb,
            macro_str_lp,
            arg_rp_marker,
            macro_str_rp,

            // ["join"](")")

            arg_get_join,
            macro_str_lp,
            macro_seq_functext,
            macro_seq_get_20,
            macro_str_rp,

            // )["bind"](1, <...>)

            macro_str_rp,
            arg_get_bind,
            macro_str_lp,
            arg_1, // Dummy argument
            macro_str_comma,
            macro_seq_partial_arg_values,
            macro_str_rp,
        ])}
    `;

    const most_of_replace_and_invoke_expression = replaceBlockCommentsAndWhitespace(`
        ${callFunction(
            join(
               _return, bt("["), pieces_of_replacer_expression, bt("]")
            ),
            [
                // The first argument is not used, but needs to be declared.
                "$",

                [arg_str_functext, __obj_function_anonymous_etc],

                [arg_1, __obj_1],

                // Use a string as a value to ensure the "3" in "32" can be accessed easily.
                [arg_32, btip(__obj_32)],

                [arg_20, _20],

                [arg_get_constructor, bt(`[${super_arg_constructor}]`)],

                [arg_24, __obj_24],

                [arg_get_join, bt(`[${super_arg_join}]`)],

                [arg_to, bt(super_arg_to)],

                [arg_e, bt(`${super_arg_e}`)],

                // As "[" is needed before "na" anyway, it can be prefixed here for space optimization.
                [arg_na_prefixed, bt(`[${super_arg_na}`)],

                [arg_lp_marker, bt(super_arg_lp_marker)],

                [arg_rp_marker, bt(super_arg_rp_marker)],

                [arg_names_string, bt(super_arg_names_string)],

                [arg_stage_suffixed, bt(`${super_arg_stage}[`)],

                [arg_get_bind, bt(`[${super_arg_bind}]`)],
            ]
        )}
        [${_join}]
        ${bt("")}
    `);

    const body_of_actual_first_stage_getter = join(
        _return, bt("[]"), most_of_replace_and_invoke_expression,
    );

    const super_arg_names_string_value = `
        []
        [${_concat}]
        ${interleave([
            /*
             * Arguments bound in this function.
             *
             * Argument names must be ordered in the same way as in macro_seq_partial_arg_values.
             *
             */

            FIRST_STAGE_ARG_CONSTRUCTOR_WRAPPED, // ["constructor"]
            FIRST_STAGE_ARG_1,                   // 1
            FIRST_STAGE_ARG_NAME,                // "name"
            FIRST_STAGE_ARG_TO,                  // "to"
            FIRST_STAGE_ARG_2E1,                 // "2e1"

            /*
             * Dummy argument to make invocation with `<...>` - as opposed to (<...>) - possible.
             *
             */

            "$",

            /*
             * For these arguments, their values are supplied in the "printProgram" function. The order must match.
             *
             */

            FIRST_STAGE_ARG_CONCAT,           // "concat"
            FIRST_STAGE_ARG_L,                // "l"
            FIRST_STAGE_ARG_RETURN_AND_SPACE, // "return "
            FIRST_STAGE_ARG_SECOND_STAGE,     // string containing the encoded second stage
            FIRST_STAGE_ARG_D,                // "d"
            FIRST_STAGE_ARG_USER_CODE,        // string containing the encoded user code

            /*
             * The dummy "" is needed to "balance" the argument list, or the middle of the function signature would
             * contain ",," and yield a syntax error.
             *
             * This actually results in the argument list ending with a single trailing comma instead, but that is
             * allowed.
             *
             */

            ""
        ])}
    `;

    const first_stage = callFunction(
        body_of_actual_first_stage_getter,
        [
            // The first argument is not used, but needs to be declared.
            "$",

            [super_arg_constructor, _constructor],

            [super_arg_join, _join],

            [super_arg_to, _to],

            [super_arg_e, _e],

            [super_arg_na, _na],

            [super_arg_lp_marker, bt(FIRST_STAGE_LP_MARKER)],

            [super_arg_rp_marker, bt(FIRST_STAGE_RP_MARKER)],

            [super_arg_names_string, super_arg_names_string_value],

            [super_arg_stage, getFirstStageTemplate()],

            [super_arg_bind, _bind],
        ]
    );

    return first_stage;
}

function getSecondStage(user_code_alphabet) {
    /*
     * Build the decoder that obtains the user code.
     *
     * Approach in brief:
     *
     * 1) Decode every input character two bits at a time.
     *
     * 2) When eight bits have been obtained, concatenate "%xx" to __obj_output.
     *
     * 3) When __obj_output is ready, decode it with decodeURIComponent and return the result so the caller can invoke
     *    the user code. (Note that an unrelated decodeURIComponent call is also used for helping to decode the second
     *    stage itself, see comment below.)
     *
     * This version of the second stage can be optimized a little for output size. Some tradeoffs have deliberately
     * been made to improve memory consumption and processing speed, among other things.
     *
     */

    const stage = replaceBlockCommentsAndWhitespace(`
        /*
         * Since the stage is encoded backwards, the null character is the last one to be decoded, and this makes it
         * easy and cheap to stop decoding at the right time.
         *
         */
        \0

        /*
         * "return " is prefixed in the first stage.
         *
         * The caller is decoding the second stage with decodeURI, which leaves a bunch of %xx leftovers.
         * decodeURIComponent takes care of all of these.
         *
         * Exception: "%" is translated to "%25" as explained below.
         *
         */
        decodeURIComponent\`

            /* "return " is prefixed in the first stage also at this point. */

            (__obj_input, __obj_byteIndex = 0, __obj_charCode = 0, __obj_output = "") => {

                /*
                 * The termination condition (check of the __obj_charCode value) runs first, then the body of the loop
                 * (__obj_charCode update), then __obj_output is updated every fourth iteration.
                 *
                 * The loop terminates when the string ends.
                 *
                 */

                for (
                    ;

                    /*
                     * This is 0 only when __obj_charCode is -1, which is what the "indexOf" function returns when it
                     * finds no match. Thus the for loop terminates at that point.
                     *
                     */

                    ~__obj_charCode

                    ;

                    /*
                     * The tertiary operator yields more efficiency space-wise than "||" for instance (the latter
                     * requires adding parentheses to make the assignment make any syntactic sense).
                     *
                     * When the last two bits in __obj_byteIndex are 11 (before the increment), this means
                     * __obj_charCode was updated four times already (one for each of <...>00, <...>01, <...>10, and
                     * <...>11), and thus the entire character code is available.
                     *
                     */

                    ++__obj_byteIndex & 3
                        ? 0 /* Dummy item added for syntax reasons */
                        : __obj_output +=

                            "%"

                            /* Left pad the character code if needed. */
                            + (__obj_charCode < 16 ? "0" : "")

                            /*
                             * Add the hexadecimal value of the char code.
                             *
                             * Pass along a dummy argument to toString to efficiently reset the char code.
                             *
                             */

                            + __obj_charCode.toString(16, __obj_charCode = 0)
                )

                    __obj_charCode =
                        __obj_charCode * 4
                        + "${user_code_alphabet}".indexOf(__obj_input[__obj_byteIndex])

                ;

                return /*space*/ decodeURIComponent(__obj_output)

            }

        \`

        /*
         * As the second stage is encoded in reverse, this is the first character.
         *
         * It is chosen in such a way that the first character in the encoded second stage is a left curly bracket,
         * which is then used in the first stage for space optimization purposes (accessing it with a simple [0]
         * indexing is cheap compared to many other ways of getting a hold of that character there).
         *
         */

        /*space*/
    `)

    // Use shortest possible variable names
    .replace(/__obj_byteIndex/g, "b")
    .replace(/__obj_charCode/g, "c")
    .replace(/__obj_input/g, "i")
    .replace(/__obj_output/g, "o")

    // Encode "%" since it would interfere with decodeURIComponent otherwise.
    .replace(/%/g, "%25")

    ;

    return Buffer
        .from(stage)
        /*
         * For technical reasons, the stage is encoded backwards. This allows a slight space optimization in the first
         * stage.
         *
         */
        .reverse()
    ;
}

function getUserCodeAlphabet() {
    /*
     * Define the user code alphabet in such a way that the first character of the encoded user code is target_char.
     *
     * This makes it possible for the first stage to get access to that literal character in a cheap way.
     *
     */

    const target_char = "}";

    const new_pos = USER_CODE[0] >> 6; // Match the code that the first two bits of the user code make up
    const old_pos = USER_CODE_ALPHABET_TEMPLATE.indexOf(target_char);

    const arr = USER_CODE_ALPHABET_TEMPLATE.split("");
    arr[old_pos] = USER_CODE_ALPHABET_TEMPLATE[new_pos];
    arr[new_pos] = target_char;

    return arr.join("");
}

/*
 * Take two bits at a time from buf_in (most significant bit pair first), turn each of those bit pairs into a character
 * using the supplied alphabet, then write to standard out in chunks using a buffer for performance.
 *
 */
function printCode(buf_in, alphabet) {
    const max_buf_size = 4 * 1024;

    const alphabet_codes = Buffer.from(alphabet);
    const buf_in_length = buf_in.length;
    const buf_out = Buffer.allocUnsafe(max_buf_size);
    let buf_out_idx = 0;

    for (let buf_in_idx = 0; buf_in_idx < buf_in_length; ++buf_in_idx) {
        let code = buf_in[buf_in_idx];

        // Intentional code duplication for speed.

        buf_out[buf_out_idx + 3] = alphabet_codes[code & 3];
        code >>>= 2;

        buf_out[buf_out_idx + 2] = alphabet_codes[code & 3];
        code >>>= 2;

        buf_out[buf_out_idx + 1] = alphabet_codes[code & 3];
        code >>>= 2;

        buf_out[buf_out_idx] = alphabet_codes[code];

        buf_out_idx += 4;

        if (buf_out_idx === max_buf_size) {
            process.stdout.write(buf_out);
            buf_out_idx = 0;
        }
    }

    process.stdout.write(buf_out.slice(0, buf_out_idx)); // Print leftovers, if any.
}

function printProgram() {
    if (USER_CODE.length === 0) {
        /*
         * This already uses at most 7 characters, so output nothing! Also: the program assumes that the user code
         * starts with "{", see for instance the "getUserCodeAlphabet" function.
         *
         */
        return;
    }

    /*
     * The "getBoundFirstStage" function returns a bound function. In particular it presupplies some arguments
     * to the first stage. The remaining arguments are supplied here.
     *
     * The order needs to match the thid part of "super_arg_names_string_value".
     *
     */

    const STAGE_MARKER = "X";

    const prog = replaceBlockCommentsAndWhitespace(`
        ${BOUND_FIRST_STAGE}
        ${join(
            preventFlatten(_concat),
            preventFlatten(_l),
            preventFlatten(join(_return, __str_space)),
            preventFlatten(bt(STAGE_MARKER)),
            preventFlatten(_d),
            preventFlatten(bt(STAGE_MARKER))
        )}
    `);

    const pieces = prog
        .replace(RegExp(FLATTENING_INHIBITOR_CHAR, "g"), "")
        .split(STAGE_MARKER)
    ;

    // Produce the output in several steps for performance reasons.

    process.stdout.write(pieces[0]);

    printCode(SECOND_STAGE, SECOND_STAGE_ALPHABET);

    process.stdout.write(pieces[1]);

    printCode(USER_CODE, getUserCodeAlphabet());

    process.stdout.write(pieces[2]);
}

/********/

const ALLOWED_ALPHABET = "`<${}[]";

/*
 * Assumptions are hardcoded elsewhere about which characters this contains and in what order.
 *
 * See the "getFirstStageTemplate" function for details (where ##__obj_charCodeBits gets updated).
 *
 */
const SECOND_STAGE_ALPHABET = "{}][";

/*
 * Characters used to encode user code. At least one more can be added, but at the cost of much increased complexity.
 *
 * The actual order of the characters is determined semi-dynamically - see getUserCodeAlphabet - so the constant itself
 * can be reordered if so desired. The characters can also be changed, but one character will be replaced with "}" if
 * it's not already present.
 *
 */
const USER_CODE_ALPHABET_TEMPLATE = "[]{}";

/*
 * These character combinations cannot be used elsewhere in the first stage code for syntax reasons, so they serve as
 * cheap markers for left parentheses and right parentheses respectively.
 *
 */
const FIRST_STAGE_LP_MARKER = "[<";
const FIRST_STAGE_RP_MARKER = "[}";

const FIRST_STAGE_ARG_CONCAT                    = "$";
const FIRST_STAGE_ARG_CONSTRUCTOR_WRAPPED       = "$$";
const FIRST_STAGE_ARG_1                         = "$$$";
const FIRST_STAGE_ARG_2E1                       = "$$$$";
// See comments in the "getFirstStageTemplate" function for an explanation of this name duplication.
const FIRST_STAGE_ARG_L                         = "$$$$$";
const FIRST_STAGE_OBJ_CODEURI_AND_OUTPUT_STRING = "$$$$$";
const FIRST_STAGE_ARG_RETURN_AND_SPACE          = "$$$$$$";
const FIRST_STAGE_ARG_NAME                      = "$$$$$$$";
const FIRST_STAGE_OBJ_RECURSIVE_FUNCTION        = "$$$$$$$$";
const FIRST_STAGE_ARG_TO                        = "$$$$$$$$$";
const FIRST_STAGE_ARG_SECOND_STAGE              = "$$$$$$$$$$";
const FIRST_STAGE_ARG_D                         = "$$$$$$$$$$$";
const FIRST_STAGE_ARG_USER_CODE                 = "$$$$$$$$$$$$";

/*
 * The inhibitor character confuses Joined.prototype.toString, and thus it eludes capture until it gets stripped out in
 * the "printProgram" function.
 *
 */
const FLATTENING_INHIBITOR_CHAR = "\0";

/* Atoms */

const __obj_an_Array = "[]"; // String representation: ""

const __obj_an_Object = "{}"; // String representation: "[object Object]"

const __obj_false = `${__obj_an_Array} < ${__obj_an_Array}`;

const __obj_true = `${__obj_an_Array} < ${__obj_an_Object}`;

// Strictly speaking, this assumes Array.prototype[""] is undefined. That should be a fairly safe assumption.
const __obj_undefined = `${__obj_an_Array}[${__obj_an_Array}]`;

/* Single digit numbers (and a bonus item used elsewhere) */

const __obj_0 = `${__obj_an_Array} << ${__obj_an_Array}`;

// Bonus
const __obj_true_bracketed = `[${__obj_true}][${__obj_0}]`;

const __obj_1 = `${__obj_true_bracketed} << ${__obj_an_Array}`;

const __obj_2 = `${__obj_true_bracketed} << ${__obj_true_bracketed}`;

/* Two-digit numbers */

// The string "10"
const _10 = join(__obj_1, __obj_0);

const _20 = join(__obj_2, __obj_0);

const __obj_24 = `${join(__obj_1, __obj_2)} << ${__obj_true_bracketed}`;

// 1 << 101 === 1 << 101 % 32 === 1 << 5 === 32
const __obj_32 = `${__obj_true_bracketed} << ${join(__obj_1, __obj_0, __obj_1)}`;

/* Space and lower-case letters */

const __str_space = `${btip(fill(3), __obj_an_Object)}[${_10}]`;

const _a = `${btip(__obj_false)}[${__obj_1}]`;

const _b = `${btip(__obj_an_Object)}[${__obj_2}]`;

const _c = `${btip(fill(5), __obj_an_Object)}[${_10}]`;

const _d = `${btip(__obj_undefined)}[${__obj_2}]`;

const _e = `${btip(fill(6), __obj_an_Object)}[${_10}]`;

const _f = `${btip(__obj_false)}[${__obj_0}]`;

const _i = `${btip(fill(5), __obj_undefined)}[${_10}]`;

const _j = `${btip(__obj_an_Object)}[${_10}]`;

const _l = `${btip(__obj_false)}[${__obj_2}]`;

const _n = `${btip(__obj_undefined)}[${__obj_1}]`;

const _o = `${btip(__obj_an_Object)}[${__obj_1}]`;

const _r = `${btip(__obj_true)}[${__obj_1}]`;

const _s = `${btip(fill(7), __obj_false)}[${_10}]`;

const _t = `${btip(__obj_true)}[${__obj_0}]`;

const _u = `${btip(__obj_undefined)}[${__obj_0}]`;

/* More complex object constructions */

const _bind = join(_b, _i, _n, _d);

const _concat = join(_c, _o, _n, _c, _a, _t);

const _constructor = join(_c, _o, _n, _s, _t, _r, _u, _c, _t, _o, _r);

const _join = join(_j, _o, _i, _n);

const _na = join(_n, _a);

const _return = join(_r, _e, _t, _u, _r, _n);

/*
 * "Random" name of a function on the Array prototype.
 *
 * Something like "at" would be considerably cheaper than "sort", but this requires ES2022.
 *
 * "flat" would be somewhat cheaper than "sort", but this extends the runtime dependency of ES2019-level features.
 *
 */
const _sort = join(_s, _o, _r, _t);

const _to = join(_t, _o);

const __obj_Function = `${__obj_an_Array}[${_sort}][${_constructor}]`; // Needed by createFunction

/*
 * The string representation of what Function() returns is well defined in ES2019, enabling a number of tricks. In
 * earlier specifications, this is not the case. (This does however not rule out that some pre-ES2019 JS engines do
 * the same thing, or something similar enough that this still works.)
 *
 * The following expression yields: "[]function anonymous($$$,$$$$$$\n) {\n\n}"
 *
 * This is exploited in the "getBoundFirstStage" function with the variable arg_str_functext, where it's used as a
 * single source for the following characters:
 *
 * - "[" (normally can be sourced directly, but that is "problematic" in this case)
 * - "]" (ditto)
 * - "m"
 * - "("
 * - ","
 * - ")"
 *
 * "(" and ")" are also used to generate "real" parentheses in that function. See e.g. usages of "macro_seq_functext".
 *
 * The dollar signs are there just to position the comma and the right parenthesis into the "right" places.
 *
 * Prefixing the string with the two square brackets also serves a similar function for "m" and the left parenthesis.
 *
 */
const __obj_function_anonymous_etc = btip("[]", createFunction("[]", ["$$$", "$$$$$$"]));

/********/

if (process.argv.length !== 3) {
    console.warn(`Usage: node j5t FILENAME`);
    console.warn();
    console.warn(`The generated code is written to standard out.`);
    process.exit(1);
}

const FILENAME = process.argv[2];
const USER_CODE = fs.readFileSync(FILENAME);
const USER_CODE_ALPHABET = getUserCodeAlphabet();

const BOUND_FIRST_STAGE = getBoundFirstStage();
const SECOND_STAGE = getSecondStage(USER_CODE_ALPHABET);

printProgram();
